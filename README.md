# linux4minix

The device boots with android. 

Use the SD image to boot Ubuntu/Debian/... the minix device.

It works by default with armv7l (or armhf).



# Media

![](medias/minix-armv7l-1.png)

![](medias/minix-armv7l-2.png)

# References

https://thomasheinz.net/tag/minix/
